package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func dump() {
	files, err := ioutil.ReadDir("/static/")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println(file.Name(), file.IsDir())
	}
}

func main() {
	e := echo.New()

	root := os.Getenv("root")
	if root == "" {
		root = "/static"
	}

	e.Use(middleware.Logger())
	e.Static(root, "/static")
	dump()

	e.Logger.Fatal(e.Start(":3000"))
}
