ARG GOLANG_VERSION=1.22.3

FROM --platform=${BUILDPLATFORM} golang:${GOLANG_VERSION}-alpine as builder

ARG TARGETARCH
ENV GOARCH="${TARGETARCH}" 
ENV GOOS=linux 
ENV CGO_ENABLED=0

RUN apk update && \
    apk add git

WORKDIR /build

COPY go.* /build/

RUN go mod download -x

COPY . /build/

RUN go build -ldflags="-w -s" .



FROM scratch

WORKDIR /

COPY --from=builder /build/staticsrv /

EXPOSE 3000

ENTRYPOINT [ "/staticsrv" ]